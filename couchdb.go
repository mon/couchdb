package couchdb

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var Verbose bool

type Client struct {
	url  *url.URL
	http *http.Client
}

type Options struct {
	Keys   []string
	Limit  int
	Group  bool
	Reduce bool
}

func (o *Options) keysJSONArrayString() string {
	if len(o.Keys) == 0 {
		return `[]`
	}

	return `["` + strings.Join(o.Keys, `", "`) + `"]`
}

func NewClient(u string) (*Client, error) {
	if Verbose {
		log.Println("NewClient: Creating client:", u)
	}

	c := &Client{
		http: &http.Client{
			Timeout: time.Second * 8,
		},
	}
	cu, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	c.url = cu

	return c, nil
}

func (c *Client) Ping() error {
	resp, err := http.Get(c.url.String())
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	return nil
}

func (c *Client) String() string {
	return c.url.String()
}

func (c *Client) Host() string {
	return c.url.Scheme + "://" + c.url.Host
}

func (c *Client) DeleteDocument(doc Documenter) error {
	if Verbose {
		log.Println("DeleteDocument: deleting document:", doc.GetID(), "rev: ", doc.GetRev())
	}

	u := c.String() + "/" + doc.GetID() + "?rev=" + doc.GetRev()

	resp, err := c.request("DELETE", u, nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(string(resp.StatusCode) + " " + resp.Status)
	}

	return nil
}

func (c *Client) GetUUID() (string, error) {
	var doc struct {
		Uuids []string `json:"uuids"`
	}

	u := c.Host() + "/_uuids"

	resp, err := c.request("GET", u, nil)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "", errors.New(string(resp.StatusCode) + " " + resp.Status)
	}

	err = readBody(resp, &doc)
	if err != nil {
		return "", err
	}

	if len(doc.Uuids) != 1 {
		return "", errors.New("response length error")
	}

	if len(doc.Uuids[0]) != 32 {
		return "", errors.New("uuid length error")
	}

	return doc.Uuids[0], nil
}

func (c *Client) GetDocument(doc Documenter) error {
	if Verbose {
		log.Println("GetDocument: document:", doc.GetID())
	}
	u := c.String() + "/" + doc.GetID()

	resp, err := c.request("GET", u, nil)
	if err != nil {
		return err
	}

	return readBody(resp, &doc)
}

func (c *Client) Find(doc interface{}, query Query) error {
	var jsonResponse struct {
		Docs    json.RawMessage `json:"docs"`
		Warning string          `json:"warning"`
	}

	u := c.String() + "/_find"

	queryBody, err := json.Marshal(query)
	if err != nil {
		return err
	}

	if Verbose {
		log.Println("Find: url: "+u+" query ", string(queryBody))
	}

	resp, err := c.request("POST", u, bytes.NewReader(queryBody))
	if err != nil {
		return err
	}

	readBody(resp, &jsonResponse)

	if Verbose {
		if len(jsonResponse.Warning) > 0 {
			log.Println(jsonResponse.Warning)
		}
	}

	err = json.Unmarshal(jsonResponse.Docs, &doc)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) CreateDocument(doc Documenter) (string, error) {
	if Verbose {
		log.Println("CreateDocument: creating document-id:", doc.GetID())
	}
	u := c.String() + "/" + doc.GetID()

	json, err := json.Marshal(doc)
	if err != nil {
		return "", err
	}

	b := bytes.NewReader(json)

	resp, err := c.request("PUT", u, b)
	if err != nil {
		if Verbose {
			log.Println("ERROR: couchdb CreateDocument c.request")
		}
		return "", err
	}

	if resp.StatusCode >= 400 {
		return "", errors.New(string(resp.StatusCode) + " " + resp.Status)
	}

	fmt.Println(resp.StatusCode)

	etag := resp.Header.Get("Etag")
	if etag == "" {
		return "", errors.New("couchdb missing Etag")
	}

	rev := etag[1 : len(etag)-1]

	if Verbose {
		log.Println("CreateDocument: got rev:", rev)
	}

	resp.Body.Close()

	return rev, nil
}

func readBody(resp *http.Response, v interface{}) error {
	if Verbose {
		log.Println("readBody: reading body")
	}

	if err := json.NewDecoder(resp.Body).Decode(&v); err != nil {
		resp.Body.Close()
		return err
	}
	return resp.Body.Close()
}

func (c *Client) request(method, path string, body io.Reader) (*http.Response, error) {
	if Verbose {
		log.Println("request: sending request:", method, path)
	}

	req, err := http.NewRequest(method, path, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode >= 400 {
		resp.Body.Close()
		return nil, errors.New(string(resp.StatusCode) + " " + resp.Status)
	}

	return resp, nil
}

func (c *Client) PutAttachment(doc Documenter, att *Attachment) (string, error) {
	path := c.String() + "/" + doc.GetID() + "/" + att.Name + "?rev=" + doc.GetRev()

	req, err := http.NewRequest("PUT", path, att)
	if err != nil {
		return "", err
	}

	req.Header.Add("Content-Type", att.Type)

	resp, err := c.http.Do(req)
	if err != nil {
		return "", err
	}

	if resp.StatusCode >= 400 {
		resp.Body.Close()
		return "", errors.New(resp.Status)
	}

	var respBody struct{ Rev string }

	err = readBody(resp, &respBody)
	if err != nil {
		return "", err
	}

	return respBody.Rev, nil
}

func (c *Client) GetAttachment(doc Documenter, att *Attachment) error {
	if att.Name == "" {
		return errors.New("Attachment name error (empty)")
	}

	resp, err := c.http.Get(c.url.String() + "/" + doc.GetID() + "/" + att.Name)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	_, err = io.Copy(att, resp.Body)
	if err != nil {
		return err
	}

	att.Type = resp.Header.Get("Content-Type")

	return nil
}

func (c *Client) DeleteAttachment(doc Documenter, att *Attachment) (string, error) {
	if att.Name == "" {
		return "", errors.New("Attachment name error (empty)")
	}

	path := c.String() + "/" + doc.GetID() + "/" + att.Name + "?rev=" + doc.GetRev()
	req, err := http.NewRequest("DELETE", path, nil)
	if err != nil {
		return "", err
	}

	resp, err := c.http.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return "", nil
	}

	err = resp.Body.Close()
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 && resp.StatusCode != 202 {
		return "", errors.New(resp.Status)
	}

	rev := ""

	etag := resp.Header.Get("ETag")
	if len(etag) > 10 {
		rev = etag[1 : len(etag)-1]
	}

	if len(etag) == 0 {
		fmt.Println("ETag missing. Reading rev from body")
		var respBody struct{ Rev string }

		err = readBody(resp, &respBody)
		if err != nil {
			return "", err
		}

		rev = respBody.Rev
	}

	if len(rev) == 0 {
		return "", errors.New("missing etag")
	}

	return rev, nil
}

func (c *Client) View(designDocument, view string, rows interface{}, options Options) error {
	queryString := []string{}
	if len(options.Keys) > 0 {
		queryString = append(queryString, "keys="+options.keysJSONArrayString())
	}

	if options.Limit > 0 {
		queryString = append(queryString, "limit="+strconv.Itoa(options.Limit))
	}

	if options.Group {
		queryString = append(queryString, "group=true")
	}

	if options.Reduce {
		queryString = append(queryString, "reduce=true")
	} else {
		queryString = append(queryString, "reduce=false")
	}

	u := c.String() + "/_design/" + designDocument + "/_view/" + view + "?" + strings.Join(queryString, "&")

	var jsonResponse struct {
		TotalRows int             `json:"total_rows"`
		Offset    int             `json:"offset"`
		Rows      json.RawMessage `json:"rows"`
	}

	resp, err := c.request("GET", u, nil)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}

	err = readBody(resp, &jsonResponse)
	if err != nil {
		return err
	}

	err = json.Unmarshal(jsonResponse.Rows, &rows)
	if err != nil {
		return err
	}

	return nil
}
