package couchdb

type Document struct {
	ID  string `json:"_id,omitempty"`
	Rev string `json:"_rev,omitempty"`
}

type Documenter interface {
	GetID() string
	GetRev() string
}

func (d *Document) GetID() string {
	return d.ID
}

func (d *Document) GetRev() string {
	return d.Rev
}
