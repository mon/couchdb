package couchdb

import (
	"bytes"
)

type Attachment struct {
	Name string
	Type string
	MD5  [16]byte

	bytes.Buffer
}
