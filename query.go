package couchdb

type Query struct {
	Selector map[string]interface{} `json:"selector"`
	Fields   []string               `json:"fields,omitempty"`
	Sort     []interface{}          `json:"sort,omitempty"`
	Limit    int                    `json:"limit,omitempty"`
	Skip     int                    `json:"skip,omitempty"`
	Index    string                 `json:"use_index,omitempty"`
}

func (q *Query) AddSelector(cond string, field string, value interface{}) {
	if q.Selector == nil {
		q.Selector = make(map[string]interface{})
	}

	t := make(map[string]interface{})
	t[cond] = value

	q.Selector[field] = t
}

func (q *Query) AddSort(field, direction string) {
	t := make(map[string]string)
	t[field] = direction

	q.Sort = append(q.Sort, t)
}
